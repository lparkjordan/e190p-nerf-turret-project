# Internet Nerf Turret
This code provides everything you need to run a web server that allows remote 
control over a Nerf turret. The code is designed to be run on a Raspberry pi
running Raspbian, but should be easily adaptable for any other Linux system
that supports GPIO pins.

The code, as presented, works best with flywheel-based Nerf/Buzzbee blasters
that automatically feed darts into the flywheels.

## Table of Contents
* [Features](#Features)
* [Installation](#Installation (Raspberry Pi))
* [Usage](#Usage)

## Features
* Several scripts designed to rotate and fire the turret.
* A basic web interface allowing any user to connect to and control the 
turret via the internet.

## Installation (Raspberry Pi)
1. Install an Apache web server on your Raspberry Pi.
2. Install mjpg-streamer.
3. Install the wiring-pi package.
4. Place all code from this repository into /var/www

## Usage
Begin by attaching all of your peripherals to the Raspberry Pi. To use the
code as presented, GPIO pin 0 should be used to turn left, GPIO pin 3 should
be used to turn right, GPIO pin 5 should be used to charge your nerf gun's
flywheels, and GPIO pin 6 should be used to fire the gun. Use the command 
"/var/www/init.sh" to initialize the IO pins to the proper state.

Once the peripherals are attached and the  web server is operational,
start mjpg-streamer on your host device using the command 
"sudo /var/www/camera.sh". From here,  usage is simple: 
connect to the server via your browser of choice from any other PC. 
Once connected, go to the turret.php page. This page contains all the controls 
necessary to rotate and fire the turret, as well as a stream from your 
attached webcam. If you wish to turn off the camera, simply run the command 
"sudo /var/www/cameraOff.sh".
